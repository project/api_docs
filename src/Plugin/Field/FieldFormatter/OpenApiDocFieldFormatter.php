<?php

namespace Drupal\api_docs\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'open_api_doc_field_formatter'.
 *
 * @FieldFormatter(
 *   id = "open_api_doc_field_formatter",
 *   label = @Translation("Open API"),
 *   description = @Translation("Open API"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class OpenApiDocFieldFormatter extends FileFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * File URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface|null
   */
  private $fileUrlGenerator;

  /**
   * Constructs a OpenApiDocFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   File URL generator.
   */
  public function __construct(string $plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, string $label, string $view_mode, array $third_party_settings, FileUrlGeneratorInterface $file_url_generator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
      'api_docs_openapi_ui' => 'redoc',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $open_api_doc_uis = ['redoc' => 'Redoc UI', 'swagger' => 'Swagger UI'];
    $element['api_docs_openapi_ui'] = [
      '#type' => 'select',
      '#title' => t("Open API Doc UI"),
      '#options' => $open_api_doc_uis,
      '#default_value' => $this->getSetting('api_docs_openapi_ui'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $open_api_doc_ui = $this->getSetting('api_docs_openapi_ui');
    if ($open_api_doc_ui) {
      $summary[] = $this->t("Open API Doc UI: @open_api_doc_ui", [
        "@open_api_doc_ui" => $open_api_doc_ui,
      ]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = parent::view($items, $langcode);
    $api_docs_openapi_ui = $this->getSetting('api_docs_openapi_ui');
    if ($api_docs_openapi_ui == "redoc") {
      $elements['#attached']['library'][] = 'api_docs/api_docs.redoc';
    }
    elseif ($api_docs_openapi_ui == "swagger") {
      $elements['#attached']['library'][] = 'api_docs/api_docs.swagger_ui';
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $key => $file) {
      $file = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
      $element[$key] = [
        '#theme' => 'open_api_doc',
        '#open_api_doc_url' => $file,
        '#open_api_doc_render' => $this->getSetting('api_docs_openapi_ui'),
      ];
    }
    return $element;
  }

}
